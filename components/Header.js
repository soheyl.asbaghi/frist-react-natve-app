import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

function Header({title}) {
  return (
    <View style={style.header}>
      <Text style={style.text}>{title}</Text>
    </View>
  );
}

Header.defaultProps = {
  title: 'Shopping List',
};

const style = StyleSheet.create({
  header: {
    padding: 15,
    height: 60,
    backgroundColor:
      'background-image: linear-gradient( 109.6deg,  rgba(245,56,56,1) 11.2%, rgba(234,192,117,1) 78% )',
  },
  text: {
    fontSize: 23,
    textAlign: 'center',
    color: '#fff',
  },
});

export default Header;
