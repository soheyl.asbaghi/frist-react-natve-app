import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

function AddItem({addItem}) {
  const [text, setText] = useState('');

  const onChangeHandler = value => {
    setText(value);
  };

  return (
    <View style={style}>
      <TextInput
        placeholder="Add Item..."
        style={style.input}
        onChangeText={onChangeHandler}
      />
      <TouchableOpacity style={style.btn} onPress={() => addItem(text)}>
        <Text style={style.btnText}>
          <Icon name="plus" size={20} /> Add Item
        </Text>
      </TouchableOpacity>
    </View>
  );
}

const style = StyleSheet.create({
  input: {height: 60, padding: 8, fontSize: 16},
  btn: {
    backgroundColor:
      'background-image: linear-gradient( 109.6deg,  rgba(245,56,56,1) 11.2%, rgba(234,192,117,1) 78% )',
    padding: 9,
    margin: 5,
  },
  btnText: {color: '#fff', fontSize: 20, textAlign: 'center'},
});

export default AddItem;
